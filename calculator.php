<!DOCTYPE html>
<html>
<head>
    <title>Simple Calculator</title>
</head>
<body>
    Basic Calculator <br>
    <form action="calculator.php" method = "GET">
    <input type="number" step = "0.01" name="firstValue"/><br>
    <input type="radio" name="operation" value="add"> +<br>
    <input type="radio" name="operation" value="subtract"> -<br>
    <input type="radio" name="operation" value="multiply"> x<br>
    <input type="radio" name="operation" value="divide"> / <br>
    <input type="number" step = "0.01" name="secondValue"/> <br>
    <input type="submit"  name= "submit" value="Calculate"/><br>
    </form>
    <?php
    $first = !empty($_GET['firstValue']) ? $_GET['firstValue'] : '';
    $second = !empty($_GET['secondValue']) ? $_GET['secondValue'] : '';
    if(isset($_GET['submit'])) {
        if(!is_null($first) && !is_null($second)) {
            if($_GET['operation'] == 'add') {
                echo ($first + $second);
            }
            else if($_GET['operation'] == 'subtract') {
                echo ($first-$second);
            }
            else if($_GET['operation'] == 'multiply') {
                echo ($first*$second);
            }
            else if($_GET['operation'] == 'divide') {
                if($second != 0) {
                    echo ($first/$second);
                }
                else {
                    echo "Can't divide by zero.";
                }
            }
        }
    }

    ?>
</body>
</html>